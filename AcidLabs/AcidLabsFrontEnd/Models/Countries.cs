﻿using System.ComponentModel.DataAnnotations;

namespace AcidLabsFrontEnd.Models
{
    /// <summary>
    /// Clase para generar listado de países utilizados en la app
    /// </summary>
    public class Countries
    {
        public string CountryCode { get; set; }
        public string ShortName { get; set; }
    }

    /// <summary>
    /// Clase para utilizarlo en dropdownlist
    /// </summary>
    public class CountryName
    {
        [Display(Name = "Seleccione País")]
        public string Name { get; set; }
    }
}