﻿using System.Web.Mvc;

namespace AcidLabsFrontEnd.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}