﻿using AcidLabsFrontEnd.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AcidLabsFrontEnd.Controllers
{
    public class IndicatorsController : Controller
    {
        // GET: Indicators
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Método que retorna listado de objetos JSON
        /// </summary>
        /// <returns>Listado de objetos "Countries"</returns>
        public JsonResult GetCountries()
        {
            WebClient client = new WebClient();
            string uriApi = "http://acidlabsapiservices.azurewebsites.net/api/GetCountries";
            string jsonCountries = client.DownloadString(uriApi);

            List<Countries> countriesList = new List<Countries>();
            countriesList = JsonConvert.DeserializeObject<List<Countries>>(jsonCountries);

            return Json(countriesList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método utilizado para la validación de datos
        /// </summary>
        /// <param name="country">País</param>
        /// <param name="startYear">Año inicio</param>
        /// <param name="endYear">Año fin</param>
        /// <returns>Objeto JSON</returns>
        [HttpPost]
        public ActionResult Validacion(string country, string startYear, string endYear)
        {
            try
            {
                return Json(new
                {
                    correcto = true,
                    url = Url.Action("VisualizarIndicadores", new { country, startYear, endYear })
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    correcto = true,
                    mensaje = ex.Message
                });
            }   
        }

        /// <summary>
        /// Método que devuelve una vista, para visualización de indicadores
        /// </summary>
        /// <param name="country">País</param>
        /// <param name="startYear">Año inicio</param>
        /// <param name="endYear">Año fin</param>
        /// <returns>Vista</returns>
        public ActionResult VisualizarIndicadores(string country, string startYear, string endYear)
        {
            ViewBag.country = country;
            ViewBag.startYear = startYear;
            ViewBag.endYear = endYear;
            return View();
        }

        /// <summary>
        /// Método "Server-Side" que retorna listado de objetos JSON para indicadores de natalidad y mortalidad
        /// </summary>
        /// <param name="param">Objeto parámetros jqueryDataTable</param>
        /// <param name="country">País</param>
        /// <param name="startYear">Año inicio</param>
        /// <param name="endYear">Año fin</param>
        /// <returns>Listado de objetos JSON</returns>
        public JsonResult GetDataIndicatorsServer(jQueryDataTableParams param, string country, string startYear, string endYear)
        {
            // Obteniendo registros
            WebClient client = new WebClient();
            string uriApi = string.Format("http://acidlabsapiservices.azurewebsites.net/api/GetIndicators/{0}/{1}/{2}", country, startYear.Trim(), endYear.Trim());
            string jsonIndicators = client.DownloadString(uriApi);
            var indicatorsList = JsonConvert.DeserializeObject<List<Indicators>>(jsonIndicators);
            IQueryable<Indicators> memberCol = indicatorsList.AsQueryable();

            // Filtros
            int totalCount = memberCol.Count();
            IEnumerable<Indicators> filteredMembers = memberCol;
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                filteredMembers = memberCol
                        .Where(m => m.CountryName.Contains(param.sSearch) ||
                                    m.Year.Contains(param.sSearch) ||
                                    m.BirthRateCrude.Contains(param.sSearch) ||
                                    m.MortalityRateAdultFemale.Contains(param.sSearch) ||
                                    m.MortalityRateAdultMale.Contains(param.sSearch) ||
                                    m.MortalityRateInfant.Contains(param.sSearch) ||
                                    m.MortalityRateUnder5.Contains(param.sSearch) ||
                                    m.MortalityRate.Contains(param.sSearch));
            }

            // Orden
            var sortIdx = Convert.ToInt32(Request["iSortCol_0"]);
            Func<Indicators, string> orderingFunction =
                                (
                                m => sortIdx == 0 ? m.CountryName :
                                     sortIdx == 1 ? m.Year :
                                     sortIdx == 2 ? m.BirthRateCrude :
                                     sortIdx == 3 ? m.MortalityRateAdultFemale :
                                     sortIdx == 4 ? m.MortalityRateAdultMale :
                                     sortIdx == 5 ? m.MortalityRateInfant :
                                     sortIdx == 6 ? m.MortalityRateUnder5 :
                                  m.MortalityRate
                                 );
            var sortDirection = Request["sSortDir_0"]; // asc or desc  
            if (sortDirection == "asc")
                filteredMembers = filteredMembers.OrderBy(orderingFunction);
            else
                filteredMembers = filteredMembers.OrderByDescending(orderingFunction);
            var displayedMembers = filteredMembers
                     .Skip(param.iDisplayStart)
                     .Take(param.iDisplayLength);

            // Resultados
            var result = from a in displayedMembers
                         select new
                         {
                             a.CountryName,
                             a.Year,
                             a.BirthRateCrude,
                             a.MortalityRateAdultFemale,
                             a.MortalityRateAdultMale,
                             a.MortalityRateInfant,
                             a.MortalityRateUnder5,
                             a.MortalityRate
                         };

            // Retornando resultados JSON
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalCount,
                iTotalDisplayRecords = filteredMembers.Count(),
                aaData = result
            },JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método "Client-Side" que retorna listado de objetos JSON para indicadores de natalidad y mortalidad
        /// </summary>
        /// <param name="param">Objeto parámetros jqueryDataTable</param>
        /// <param name="country">País</param>
        /// <param name="startYear">Año inicio</param>
        /// <param name="endYear">Año fin</param>
        /// <returns>Listado de objetos JSON</returns>
        public ActionResult GetDataIndicatorsClient(jQueryDataTableParams param, string country, string startYear, string endYear)
        {
            // Obteniendo registros
            WebClient client = new WebClient();
            string uriApi = string.Format("http://acidlabsapiservices.azurewebsites.net/api/GetIndicators/{0}/{1}/{2}", country, startYear.Trim(), endYear.Trim());
            string jsonIndicators = client.DownloadString(uriApi);
            var indicatorsList = JsonConvert.DeserializeObject<List<Indicators>>(jsonIndicators);

            // Retornando resultados JSON
            var jsonResult = Json(new { aaData = indicatorsList }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}