﻿using AcidLabsApiServices.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;

namespace AcidLabsApiServices.Controllers
{
    public class AzureServicesController : ApiController
    {
        #region Constantes

        private readonly string strConnection = @"Server=tcp:facturayasrvcrt.database.windows.net,1433;Initial Catalog=acidlabsdb;Persist Security Info=False;User ID=acidlabs;Password=AcidLab$Y2017$*;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        #endregion

        #region Métodos

        /// <summary>
        /// Método que obtiene una lista de todos los países 
        /// </summary>
        /// <returns>Lista de objetos "Countries"</returns>
        [Route("api/GetCountries")]
        public List<Countries> GetCountries()
        {
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = strConnection;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "GetCountries";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            reader = sqlCmd.ExecuteReader();

            List<Countries> countries = new List<Countries>() {
                new Countries
                {
                    CountryCode = "ALL",
                    ShortName = "Todos"
                }
            };

            Countries country = null;
            while (reader.Read())
            {
                country = new Countries();
                country.CountryCode = reader.GetValue(0).ToString();
                country.ShortName = reader.GetValue(1).ToString();
                countries.Add(country);
            }

            myConnection.Close();
            return countries;
        }

        /// <summary>
        /// Método que retorna una lista de indicadores
        /// de tasas de natividad y mortalidad
        /// </summary>
        /// <param name="countryCode">Código de país</param>
        /// <param name="startYear">Año inicio</param>
        /// <param name="endYear">Año fin</param>
        /// <returns>Lista de objetos "Indicators"</returns>
        [Route("api/GetIndicators/{countryCode}/{startYear}/{endYear}")]
        public List<Indicators> GetIndicators(string countryCode, string startYear, string endYear)
        {
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = strConnection;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "GetIndicators";
            sqlCmd.Connection = myConnection;
            sqlCmd.Parameters.AddWithValue("@CountryCode", countryCode);
            sqlCmd.Parameters.AddWithValue("@StartYear", startYear);
            sqlCmd.Parameters.AddWithValue("@EndYear", endYear);

            myConnection.Open();
            reader = sqlCmd.ExecuteReader();

            List<Indicators> indicators = new List<Indicators>();
            Indicators indicator = null;
            while (reader.Read())
            {
                indicator = new Indicators();
                indicator.CountryName = reader.GetValue(0).ToString();
                indicator.Year = reader.GetValue(1).ToString();
                indicator.BirthRateCrude = reader.GetValue(2).ToString();
                indicator.MortalityRateAdultFemale = reader.GetValue(3).ToString();
                indicator.MortalityRateAdultMale = reader.GetValue(4).ToString();
                indicator.MortalityRateInfant = reader.GetValue(5).ToString();
                indicator.MortalityRateUnder5 = reader.GetValue(6).ToString();
                indicator.MortalityRate = reader.GetValue(7).ToString();
                indicators.Add(indicator);
            }

            myConnection.Close();
            return indicators;
        }

        #endregion
    }
}
