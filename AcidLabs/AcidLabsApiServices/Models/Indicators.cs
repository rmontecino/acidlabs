﻿namespace AcidLabsApiServices.Models
{
    /// <summary>
    /// Clase para generar listados de indicadores de natalidad y mortalidad
    /// </summary>
    public class Indicators
    {
        public string CountryName { get; set; }
        public string Year { get; set; }
        public string BirthRateCrude { get; set; }
        public string MortalityRateAdultFemale { get; set; }
        public string MortalityRateAdultMale { get; set; }
        public string MortalityRateInfant { get; set; }
        public string MortalityRateUnder5 { get; set; }
        public string MortalityRate { get; set; }
    }
}