﻿namespace AcidLabsApiServices.Models
{
    /// <summary>
    /// Clase para generar listado de países utilizados en la app
    /// </summary>
    public class Countries
    {
        public string CountryCode { get; set; }
        public string ShortName { get; set; }
    }
}